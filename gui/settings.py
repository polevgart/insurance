import time
import logging
import itertools
import tkinter as tk
from tkinter import ttk
from ..libs import experiment as ex
from PIL import Image, ImageTk
from .settings_lib import Settings, ConfigEntry
from ..constants import insurance_types

logger = logging.getLogger(__name__)


def configure_style():
    style = tk.ttk.Style()
    style.configure("TButton", padding=6, relief="flat", background="#4e4", font="arial 26")
    style.configure("TLabel", foreground="white", background="black")
    style.configure("TFrame", background="#f00")


def is_natural(s):
    try:
        x = int(s)
    except ValueError:
        return False
    return x


def to_float(s):
    try:
        x = float(s)
    except ValueError:
        raise ValueError("Ожидается число")
    return x


class CommonSettings(Settings):
    def __init__(self, parent, change_state_start_btn):
        configs_entry = (
            ConfigEntry("Стартовый капитал, в у.е.", (30000,), validator=lambda x: 0 <= x),
            ConfigEntry("Продолжительность моделирования, в месяцах (от 1 до 24)", (13,), validator=lambda x: 1 <= x <= 24),
            ConfigEntry("Налог на капитал компании, в %", (9.0,), converter=float, validator=lambda x: 0 <= x <= 100),
        )
        super().__init__(parent, "Общие параметры", configs_entry, change_state_btn=change_state_start_btn)

    @property
    def money(self):
        return int(self[0])

    @property
    def duration(self):
        return int(self[1])

    @property
    def tax_rate(self):
        value = to_float(self[2])
        return value


class InsuranceSettings(Settings):
    def __init__(self, parent, type_insurance, change_state_start_btn):
        self.type_insurance = type_insurance
        configs_entry = (
            ConfigEntry("Число подписанных договоров в месяц", (0, 15), validator=lambda x: 0 <= x),
            ConfigEntry("Число страховых случаев в месяц", (1, 25), validator=lambda x: 0 <= x),
        )
        super().__init__(parent, type_insurance, configs_entry, change_state_btn=change_state_start_btn)

    @property
    def availible_current_demand(self):
        return self[0]

    @property
    def number_insured_events_per_month(self):
        return self[1]


class SettingsScreen:
    def __init__(self, root, width=907, height=510):
        self.root = root
        self.width = int(width * 1.3)
        self.height = int(height * 1.3)

        self.common_settings = None
        self.insurance_settings = None
        self.start_button = None
        self.img = None
        self.img_bg = None
        self.settings_background = None

        self.img = Image.open("insurance/pictures/bones.webp").transpose(Image.FLIP_LEFT_RIGHT)
        self.background = tk.Label()
        self.common_settings = CommonSettings(self.root, self.change_state_start_btn)
        self.insurance_settings = tuple(map(lambda x: InsuranceSettings(self.root, x, self.change_state_start_btn), insurance_types))

        self.start_button = ttk.Button(text="Начать", width=10)
        self.hideble_objects = tuple(itertools.chain((self.start_button, self.common_settings), self.insurance_settings))

    def set_link_to_game_screen(self, game_screen):
        self.game_screen = game_screen
        self.start_button["command"] = lambda: self.start()

    def start(self):
        if self.common_settings.validate() and all(map(lambda x: x.validate(), self.insurance_settings)):
            ex.EXPERIMENT = ex.Experiment.construct_from_gui(self.common_settings, self.insurance_settings)
            self.place_forget()
            self.game_screen.show()
            self.game_screen.add_stat()
        else:
            print("F")

    def show(self):
        configure_style()
        self.root.title("Начальные параметры")

        logger.debug("Placing common settings...")
        x, y = 10, 10
        dy = 50
        self.common_settings.place(x=x, y=y)
        self.root.update()
        y += self.common_settings.winfo_height() + dy
        for setting in self.insurance_settings:
            logger.debug("Placing insurance settings...")
            setting.place(x=x, y=y)
            self.root.update()
            y += setting.winfo_height() + dy
            _x = x + setting.winfo_width() + dy

        logger.debug("Placing button...")
        self.start_button.place(x=0, y=0)
        self.root.update()
        self.start_button.place(x=_x - dy//2, y=y - dy - self.start_button.winfo_height() - 5)

        self.width = int(self.width / self.height * y)
        self.height = y

        self.root.geometry("{}x{}".format(self.width, self.height))
        self.root.update()
        x = (self.root.winfo_screenwidth() - self.root.winfo_width()) // 2
        y = (self.root.winfo_screenheight() - self.root.winfo_height()) // 2
        self.root.wm_geometry("+%d+%d" % (x, y))

        logger.debug("Placing background...")
        resized_img = self.img.resize((self.width, self.height), Image.ANTIALIAS)
        self.settings_background = ImageTk.PhotoImage(resized_img)
        self.background["image"] = self.settings_background
        self.background.place(x=0, y=0, width=self.width, height=self.height)

    def place_forget(self):
        for obj in self.hideble_objects:
            obj.place_forget()
        self.background.place_forget()

    def change_state_start_btn(self):
        logger.info("GG")
        for setting in itertools.chain(self.insurance_settings, [self.common_settings]):
            if not setting.validate():
                self.start_button["state"] = "disable"
                return
        self.start_button["state"] = "default"
