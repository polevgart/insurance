import attr
import itertools
import logging
import tkinter as tk
from tkinter import ttk


logger = logging.getLogger(__name__)


@attr.s(frozen=True, slots=True)
class ConfigEntry:
    description = attr.ib(type=str)
    defaults = attr.ib()
    converter = attr.ib(default=int)
    validator = attr.ib(default=lambda x: True)


class ValidatedEntry(ttk.Entry):
    def __init__(self, parent, width, text, converter, validator, change_state_btn):
        super().__init__(parent, width=width, justify="center", font="arial 22")

        self._converter = converter
        self._validator = validator
        self._change_state_btn = change_state_btn
        self.insert(0, text)
        self['validatecommand'] = self.validate
        self['invalidcommand'] = self.invalid_cmd
        self['validate'] = 'focus'

    def validate(self):
        self["style"] = "TEntry"
        self._change_state_btn()
        return self.is_valid

    @property
    def is_valid(self):
        try:
            value = self._converter(self.get())
        except ValueError:
            return False
        return self._validator(value)

    def invalid_cmd(self):
        self._change_state_btn()
        self["style"] = "Bad.TEntry"


class Settings(tk.Frame):
    title_font = 'arial 36'
    item_font = 'arial 24'
    entry_width = 20
    entry_indent = 50
    entry_dx = 12
    dy = 2
    indent = 30

    def __init__(self, root, title, configs_entry, bg="black", label_style="TLabel", change_state_btn=lambda: None):
        super().__init__(root, bg=bg)

        fg = "white" if bg == "black" else "black"

        self.configs_entry = configs_entry
        self.title = ttk.Label(self, text=title, font=self.title_font, foreground=fg, background=bg)
        self.label_items = []
        self.input_items = []
        for config in configs_entry:
            entry_width = (self.entry_width - 1) // len(config.defaults)
            self.input_items.append(tuple(ValidatedEntry(self, entry_width, text, config.converter, config.validator, change_state_btn) for text in config.defaults))
            self.label_items.append(ttk.Label(self, text=config.description, font=self.item_font, background=bg, foreground=fg))

        self["width"], self["height"] = self._inner_place()
        self.save_changes()

    def _inner_place(self):
        self.title.place(x=0, y=0)
        self.update()
        x = self.indent
        y = self.title.winfo_height() + self.dy
        for label_item in self.label_items:
            label_item.place(x=x, y=y)
            self.update()
            y += label_item.winfo_height() + self.dy

        x += max(map(lambda z: z.winfo_width(), self.label_items)) + self.entry_indent
        y = self.title.winfo_height() + self.dy
        dy = self.label_items[0].winfo_height() + self.dy
        for input_item in self.input_items:
            _x = x
            for entry in input_item:
                entry.place(x=_x, y=y)
                self.update()
                _x += entry.winfo_width() + self.entry_dx
            y += dy
        return _x + self.entry_dx, y

    def __getitem__(self, pos):
        converter = self.configs_entry[pos].converter
        res = tuple(converter(entry.get()) for entry in self.input_items[pos])
        if len(res) == 1:
            return res[0]
        return res

    def validate(self):
        for entry in itertools.chain.from_iterable(self.input_items):
            if not entry.is_valid:
                return False
        return True

    def __del__(self):
        logging.warning("Not deleted")

    def cancel_changes(self):
        for text, entry in zip(self.backup, itertools.chain.from_iterable(self.input_items)):
            entry.delete(0, 10000)
            entry.insert(0, text)
            entry.validate()

    def save_changes(self):
        self.backup = [entry.get() for entry in itertools.chain.from_iterable(self.input_items)]
