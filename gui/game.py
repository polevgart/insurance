import logging
import tkinter as tk
from tkinter import ttk
from ..libs import experiment as ex
from ..constants import CommonStatHeads, InsuranceStatHeads, insurance_types
from .settings_lib import Settings, ConfigEntry


logger = logging.getLogger(__name__)


def configure_style(root):
    style = tk.ttk.Style()
    font="arial 18"
    style.configure("TButton", padding=6, relief="flat", background="#4e4", font=font)
    style.configure("Yellow.TButton", padding=6, relief="flat", background="yellow", font=font)
    style.configure("Red.TButton", padding=6, relief="flat", background="red", font=font)
    style.configure("TLabel", foreground="black", background=root["bg"])
    style.configure("TLabelSettings", foreground="white", background="black")
    style.configure("Treeview", font='arial 16', rowheight=25)
    style.configure("Heading", font='arial 20')


class Stat(tk.Frame):
    title_font = 'arial 30'
    item_font = 'arial 24'
    table_indent = 30

    def __init__(self, parent, title, width, height, headings=tuple()):
        super().__init__(parent)

        self.width = width
        self.height = height

        self.title = ttk.Label(self, text=title, font=self.title_font)
        self.title.place(x=0, y=0)
        self.update()

        self.table = None
        self.headings = headings

        self.reset()

    def place(self, x, y):
        super().place(x=x, y=y, width=self.width, height=self.height)

    def add_column(self, col_name, col):
        if str(col_name) == "0":
            col_name = "Начало"
        elif col_name:
            col_name = "{}й месяц".format(col_name)

        for row in self.table.get_children():
            self.table.delete(row)

        self.columns.append(col_name)
        self.table["columns"] = self.columns + ["last"]
        self.table["displaycolumns"] = self.columns + ["last"]

        self.table.heading(self.columns[0], text=self.columns[0], anchor=tk.CENTER)
        self.table.column(self.columns[0], anchor=tk.W, width=335, stretch=False)
        for col_head in self.columns[1:]:
            self.table.heading(col_head, text=col_head, anchor=tk.CENTER)
            self.table.column(col_head, anchor=tk.E, width=135, stretch=False)


        self.table.heading("last", text="", anchor=tk.CENTER)
        self.table.column("last", anchor=tk.E, width=100, stretch=True)

        for heading in self.headings:
            self.heading2rows[heading].append(col[heading])
            self.table.insert("", tk.END, values=tuple(self.heading2rows[heading] + [""]))

    def reset(self):
        if self.table:
            self.table.destroy()
        self.table = ttk.Treeview(self, show="headings", selectmode="browse")

        self.columns = []
        self.heading2rows = {heading: list() for heading in self.headings}
        self.add_column("", {heading: heading for heading in self.headings})

        xsb = ttk.Scrollbar(self.table, orient=tk.HORIZONTAL, command=self.table.xview)
        self.table["xscroll"] = xsb.set
        xsb.pack(side=tk.BOTTOM, fill=tk.X)

        ysb = ttk.Scrollbar(self.table, orient=tk.VERTICAL, command=self.table.yview)
        self.table["yscroll"] = ysb.set
        ysb.pack(side=tk.RIGHT, fill=tk.Y)

        self.table.place(
            x=self.table_indent,
            y=self.title.winfo_height() + 1,
            width=self.width - self.table_indent,
            height=self.height - self.title.winfo_height() - 1,
        )


class InsuranceStat(Stat):
    def __init__(self, parent, title, width):
        super().__init__(parent, title=title, headings=InsuranceStatHeads.head, width=width, height=220)


class CommonStat(Stat):
    def __init__(self, parent, width):
        super().__init__(parent, title="Общая статистика", headings=CommonStatHeads.head, width=width, height=220)


class GameScreen:
    def __init__(self, root, width=1800, height=875):
        configure_style(root)
        self.root = root
        self.width = width
        self.height = height

        self.next_button = ttk.Button(text="Следующий месяц", width=20)
        self.settings_button = ttk.Button(text="Изменить договоры", width=20, command=self.settings_callback, style="Yellow.TButton")
        self.restart_button = ttk.Button(text="Перезапуск", width=20, style="Red.TButton")
        self.complete_button = ttk.Button(text="Автозавершение", width=20)


        self.common_stat = CommonStat(self.root, width=self.width - 25)
        self.insurance_type2insurance_stat = {title: InsuranceStat(self.root, title=title, width=self.width - 25) for title in insurance_types}
        self.insurance_settings_screen = InsuranceSettingsScreen()

        self.game_over_label = ttk.Label(text="Game over", font="arial 42 bold", foreground="red", background="black")
        self.win_label = ttk.Label(text="You win", font="arial 42 bold", foreground="green", background="black")

        self.hideble_objects = (
            self.common_stat,
            *self.insurance_type2insurance_stat.values(),
            self.next_button,
            self.restart_button,
            self.settings_button,
            self.insurance_settings_screen,
            self.game_over_label,
            self.win_label,
            self.complete_button,
        )

    def set_link_to_settings_screen(self, settings_screen):
        self.settings_screen = settings_screen
        self.next_button["command"] = lambda: self.next_callback()
        self.restart_button["command"] = lambda: self.restart_callback()
        self.complete_button["command"] = lambda: self.complete_callback()

    def add_stat(self):
        cur_month = ex.EXPERIMENT.current_month
        self.common_stat.add_column(cur_month, ex.EXPERIMENT.get_common_stat())
        for insurance_type, insurance_stat in self.insurance_type2insurance_stat.items():
            insurance_stat.add_column(cur_month, ex.EXPERIMENT.get_insurance_stat(insurance_type))

    def next_callback(self):
        ex.EXPERIMENT.tick()
        logger.info("game over = %s; win = %s", ex.EXPERIMENT.is_game_over, ex.EXPERIMENT.is_win)
        if ex.EXPERIMENT.is_game_over or ex.EXPERIMENT.is_win:
            self.next_button.place_forget()
            self.settings_button.place_forget()
            self.complete_button.place_forget()
            self.insurance_settings_screen.place_forget()
            end_label = self.win_label if ex.EXPERIMENT.is_win else self.game_over_label
            end_label.place(x=0, y=0)
            self.root.update()
            w, h = end_label.winfo_width(), end_label.winfo_height()
            end_label.place(x=self.width//2 - w//2, y=self.height//2 - h//2)
        self.add_stat()

    def restart_callback(self):
        self.place_forget()
        reset_objects = (
            self.common_stat,
            *self.insurance_type2insurance_stat.values(),
        )
        for obj in reset_objects:
            obj.reset()
        self.settings_screen.show()

    def settings_callback(self):
        self.insurance_settings_screen.show()

    def complete_callback(self):
        while not (ex.EXPERIMENT.is_game_over or ex.EXPERIMENT.is_win):
            self.next_callback()

    def show(self):
        self.insurance_settings_screen.update_callback()
        configure_style(self.root)
        self.root.title("Компания")

        x, y = 10, 10
        dy = 10
        self.common_stat.place(x=x, y=y)
        self.root.update()
        y += self.common_stat.winfo_height() + dy
        for insurance_type in insurance_types:
            stat = self.insurance_type2insurance_stat[insurance_type]
            stat.place(x=x, y=y)
            self.root.update()
            y += stat.winfo_height() + 5

        x = self.width
        self.complete_button.place(x=0, y=0)
        self.root.update()
        dx = self.complete_button.winfo_width() + 10
        buttons = (
            self.complete_button,
            self.next_button,
            self.settings_button,
            self.restart_button,
        )
        for obj in buttons:
            x -= dx
            obj.place(x=x, y=y)

        self.root.update()
        y += buttons[0].winfo_height() + 5
        self.height = y

        self.root.geometry("{}x{}".format(self.width, self.height))
        self.root.update()
        x = (self.root.winfo_screenwidth() - self.root.winfo_width()) // 2
        y = (self.root.winfo_screenheight() - self.root.winfo_height()) // 2
        self.root.wm_geometry("+%d+%d" % (x, y))

    def place_forget(self):
        for obj in self.hideble_objects:
            obj.place_forget()

class InsuranceContractSettings(Settings):
    def __init__(self, parent, type_insurance, change_state_btn):
        self.type_insurance = type_insurance
        configs_entry = (
            ConfigEntry("Размер взноса, в у.е.", (3000,), validator=lambda x: 0 < x),
            ConfigEntry("Частота взноса, в месяцах (от 1 до 24)", (3,), validator=lambda x: 1 <= x <= 24),
            ConfigEntry("Срок действия договора, в месяцах (от 1 до 24)", (6,), validator=lambda x: 1 <= x <= 24),
            ConfigEntry("Диапазон возможных выплат, в у.е.", (1000,10000), validator=lambda x: 0 < x),
        )
        super().__init__(parent, type_insurance, configs_entry, label_style="TLabelSettings", change_state_btn=change_state_btn)

    @property
    def contribution_amount(self):
        return self[0]

    @property
    def contribution_frequency(self):
        return self[1]

    @property
    def contract_ttl(self):
        return self[2]

    @property
    def payment_amount(self):
        return self[3]


class InsuranceSettingsScreen:
    def __init__(self):
        self._bad_entry = 0
        self.root = tk.Toplevel(bg="black")
        self.root.withdraw()
        self.root.title("Параметры договоров")

        self.insurance_settings = tuple(InsuranceContractSettings(self.root, insurance_type, change_state_btn=self.change_state_update_btn) for insurance_type in insurance_types)

        self.cancel_button = ttk.Button(self.root, text="Отмена", width=20, command=self.cancel_callback, style="Red.TButton")
        self.update_button = ttk.Button(self.root, text="Обновить параметры", width=20, command=self.update_callback)
        self.hideble_objects = (
            *self.insurance_settings,
            self.cancel_button,
            self.update_button,
        )

        x, y = 10, 10
        dy = 10
        for setting in self.insurance_settings:
            setting.place(x=x, y=y)
            self.root.update()
            y += setting.winfo_height() + dy
            _x = 2 * x + setting.winfo_width()

        x = _x
        self.update_button.place(x=0, y=0)
        self.root.update()
        dx = self.update_button.winfo_width() + 10
        buttons = (
            self.update_button,
            self.cancel_button,
        )
        for btn in buttons:
            x -= dx
            btn.place(x=x, y=y)

        self.root.update()
        y += buttons[0].winfo_height() + dy

        self.root["width"], self.root["height"] = _x, y

        self.root.overrideredirect(1)
        self.root.update()
        x = (self.root.winfo_screenwidth() - self.root.winfo_width()) // 2
        y = (self.root.winfo_screenheight() - self.root.winfo_height()) // 2
        self.root.wm_geometry("+%d+%d" % (x, y))

    def show(self):
        self.root.deiconify()

    def update_callback(self):
        for setting in self.insurance_settings:
            if not setting.validate():
                logger.debug("Invalid values for '%s'", setting.type_insurance)
                return
        try:
            ex.EXPERIMENT.update_contracts_from_gui(self.insurance_settings)
            self.place_forget()
            for setting in self.insurance_settings:
                setting.save_changes()
        except ValueError:
            pass

    def cancel_callback(self):
        self.place_forget()
        for setting in self.insurance_settings:
            setting.cancel_changes()

    def change_state_update_btn(self):
        for setting in self.insurance_settings:
            if not setting.validate():
                self.update_button["state"] = "disable"
                return
        self.update_button["state"] = "default"


    def place_forget(self):
        self.root.withdraw()
