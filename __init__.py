import logging
import tkinter as tk
from .gui.settings import SettingsScreen
from .gui.game import GameScreen


def benchmark(func):
    def new_func(*args, **kwargs):
        import yappi, time
        yappi.start()
        func(*args, **kwargs)
        yappi.stop()
        yappi.get_func_stats().save("callgrind_{}.out".format(time.time()), "CALLGRIND")
    return new_func


def configure_style():
    style = tk.ttk.Style()
    style.configure("TButton", padding=6, relief="flat", background="#4e4")
    style.configure("TLabel", foreground="white", background="black")
    style.configure("Bad.TEntry", fieldbackground="#f00")


def main():
    global root, settings_screen, game_screen
    root = tk.Tk()
    logging.debug("Configure styles...")
    configure_style()
    logging.debug("Creating settings screen...")
    settings_screen = SettingsScreen(root)
    logging.debug("Creating game screen...")
    game_screen = GameScreen(root)
    logging.debug("Linking...")
    settings_screen.set_link_to_game_screen(game_screen)
    game_screen.set_link_to_settings_screen(settings_screen)
    logging.debug("Showing...")
    settings_screen.show()
    root.mainloop()


if __name__ == '__main__':
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.DEBUG,
    )
    main()
