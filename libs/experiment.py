import attr
import functools
import logging
import math
import random
import typing
from collections import Counter, defaultdict
from ..constants import insurance_types

logger = logging.getLogger(__name__)
EXPERIMENT = None


@attr.s(slots=True, kw_only=True)
class InsuranceContract:
    type_insurance = attr.ib(type=str)
    contribution_amount = attr.ib(type=int)
    contribution_frequency = attr.ib(type=int)
    contract_ttl = attr.ib(type=int)
    min_payment_amount = attr.ib(type=int)
    max_payment_amount = attr.ib(type=int)

    def sign(self, month_signing):
        return SignedInsuranceContract(**attr.asdict(self), month_signing=month_signing)

    @property
    def total_cost(self):
        return (self.contract_ttl // self.contribution_frequency + 1) * self.contribution_amount


@attr.s(slots=True, kw_only=True)
class SignedInsuranceContract(InsuranceContract):
    month_signing = attr.ib(type=int)
    next_contrubution = attr.ib(type=int, default=0)

    @property
    def deadline_month(self):
        return self.month_signing + self.contract_ttl

    def get_contribution(self):
        if self.next_contrubution == 0:
            self.next_contrubution = self.contribution_frequency - 1
            return self.contribution_amount
        self.next_contrubution -= 1
        return 0


class InsuranceCompany:
    def __init__(self, money):
        self.money = money
        self.insurance_type2contract = {}
        self._signed_contracts = []
        self.payouts = []
        self.earnings = []
        self.tax_stat = []
        self.number_insured_events = []

    def get_stat(self, month, type_insurance=None):
        contract = None
        if type_insurance:
            contract = self.insurance_type2contract[type_insurance]
        fresh_contracts = filter((lambda x: x.month_signing == month), self._signed_contracts)
        if type_insurance:
            fresh_contracts = filter(lambda x: x.type_insurance == type_insurance, fresh_contracts)
        fresh_contracts = tuple(fresh_contracts)

        stat = {
            "current_contract": contract,
            "contracts_signed": "",
            "payouts": "",
            "earnings": "",
            "tax": "",
            "number_insured_events": "",
        }
        if month >= 0:
            stat.update({
                "contracts_signed": len(fresh_contracts),
                "payouts": self.payouts[month][type_insurance],
                "earnings": self.earnings[month][type_insurance],
                "number_insured_events": self.number_insured_events[month][type_insurance],
            })
            if type_insurance is None:
                stat["tax"] = self.tax_stat[month]
        return stat

    def get_tax(self, tax_rate):
        tax = int(self.money / 100 * tax_rate)
        self.money -= tax
        self.tax_stat.append(tax)

    def sign_contracts(self, current_month, insurance_type2cnt_insurance_sold):
        for insurance_type, cnt_signed_contracts in insurance_type2cnt_insurance_sold.items():
            contract = self.insurance_type2contract[insurance_type]
            for i in range(cnt_signed_contracts):
                self._signed_contracts.append(contract.sign(current_month))

    def make_payments(self, current_month, insurance_type2number_insured_events):
        cur_payments = defaultdict(lambda: 0)
        availible_contracts = tuple(filter(lambda x: x.deadline_month >= current_month and x.month_signing < current_month, self._signed_contracts))
        for insurance_type, number_insured_events in insurance_type2number_insured_events.items():
            current_contracts = tuple(filter(lambda x: x.type_insurance == insurance_type, availible_contracts))
            insured_events = random.sample(current_contracts, min(number_insured_events, len(current_contracts)))
            for event in insured_events:
                t = random.random()
                payment = int(event.max_payment_amount * t)
                if payment >= event.min_payment_amount:
                    cur_payments[event.type_insurance] += payment

        cur_payments[None] = sum(cur_payments.values())
        self.payouts.append(cur_payments)
        self.money -= cur_payments[None]
        insurance_type2number_insured_events[None] = sum(insurance_type2number_insured_events.values())
        self.number_insured_events.append(insurance_type2number_insured_events)

    def collect_payments(self, current_month, is_game_over=False):
        present_contracts = filter(lambda x: x.deadline_month >= current_month, self._signed_contracts)
        cur_earnings = defaultdict(lambda: 0)
        for contract in present_contracts:
            cur_earnings[contract.type_insurance] += contract.get_contribution()

        cur_earnings[None] = sum(cur_earnings.values())
        if not is_game_over:
            self.money += cur_earnings[None]
        self.earnings.append(cur_earnings)


class Experiment:
    def __init__(self, duration, tax_rate, money, insurance_type2get_number_insurance_sold, insurance_type2get_number_insured_events):
        self.duration = duration
        self.tax_rate = tax_rate
        self.insurance_company = InsuranceCompany(money=money)
        self.current_month = 0
        self.insurance_type2get_number_insurance_sold = insurance_type2get_number_insurance_sold
        self.insurance_type2get_number_insured_events = insurance_type2get_number_insured_events
        self.is_game_over = False

    def tick(self):
        insurance_type2cnt_insured_events = {}
        insurance_type2cnt_insurance_sold = {}
        for insurance_type in insurance_types:
            insurance_type2cnt_insured_events[insurance_type] = self.insurance_type2get_number_insured_events[insurance_type]()
            contract = self.insurance_company.insurance_type2contract[insurance_type]
            insurance_type2cnt_insurance_sold[insurance_type] = self.insurance_type2get_number_insurance_sold[insurance_type](contract)

        self.insurance_company.get_tax(self.tax_rate)
        self.insurance_company.sign_contracts(self.current_month, insurance_type2cnt_insurance_sold)
        self.insurance_company.collect_payments(self.current_month, is_game_over=self.is_game_over)
        self.insurance_company.make_payments(self.current_month, insurance_type2cnt_insured_events)
        self.is_game_over = self.insurance_company.money < 0
        self.current_month += 1

    @staticmethod
    def construct_from_gui(common_settings, insurance_settings):
        insurance_type2get_number_insurance_sold = {}
        insurance_type2get_number_insured_events = {}

        def get_number_insurance_sold(a0, b0, contract):
            multiply = (b0 - a0 + 1) ** 0.5
            base_demand = contract.max_payment_amount / contract.total_cost
            b = max(a0, b0 - int(multiply / base_demand))
            a = min(b, max(a0, int(multiply * base_demand)))
            logger.info("%s:%s   %s  %s    %s:%s", a, b, multiply, base_demand, a0, b0)
            return random.randint(a, b)

        for setting in insurance_settings:
            insurance_type2get_number_insurance_sold[setting.type_insurance] = functools.partial(get_number_insurance_sold, *setting.availible_current_demand)
            insurance_type2get_number_insured_events[setting.type_insurance] = functools.partial(random.randint, *setting.number_insured_events_per_month)

        return Experiment(
            duration=common_settings.duration,
            tax_rate=common_settings.tax_rate,
            money=common_settings.money,
            insurance_type2get_number_insurance_sold=insurance_type2get_number_insurance_sold,
            insurance_type2get_number_insured_events=insurance_type2get_number_insured_events,
        )

    def update_contracts_from_gui(self, insurance_contracts_settings):
        self.insurance_company.insurance_type2contract = {}
        for contract_settings in insurance_contracts_settings:
            payment_amount = contract_settings.payment_amount
            contract = InsuranceContract(
                type_insurance=contract_settings.type_insurance,
                contribution_amount=contract_settings.contribution_amount,
                contribution_frequency=contract_settings.contribution_frequency,
                contract_ttl=contract_settings.contract_ttl,
                min_payment_amount=payment_amount[0],
                max_payment_amount=payment_amount[1],
            )
            self.insurance_company.insurance_type2contract[contract_settings.type_insurance] = contract
        logger.debug("update ok")

    def get_common_stat(self):
        stat = self.insurance_company.get_stat(self.current_month - 1)
        return {
            "Текущий капитал": self.insurance_company.money,
            "Кол-во проданных страховок": stat["contracts_signed"],
            "Выплаты по страховым случаям": stat["payouts"],
            "Денег заработано": stat["earnings"],
            "Налог": stat["tax"],
            "Число страховых случаев": stat["number_insured_events"],
            "":"",
        }

    def get_insurance_stat(self, insurance_type):
        stat = self.insurance_company.get_stat(self.current_month - 1, insurance_type)
        contract = stat["current_contract"]
        return {
            "Размер взноса": contract.contribution_amount,
            "Частота взноса": contract.contribution_frequency,
            "Срок действия договора": contract.contract_ttl,
            "Диапазон возможных выплат": "{}-{}".format(contract.min_payment_amount, contract.max_payment_amount),
            "Договоров подписано": stat["contracts_signed"],
            "Число страховых случаев": stat["number_insured_events"],
            "Денег выплачено": stat["payouts"],
            "Денег заработано": stat["earnings"],
            "": "",
        }

    @property
    def is_win(self):
        return self.duration <= self.current_month
